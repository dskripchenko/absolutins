<?php

namespace App\Services\Article;

use App\Interfaces\Article\ArticleServiceInterface;
use App\Models\Article;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

/**
 * Class ArticleService
 * @package App\Services\Article
 */
class ArticleService implements ArticleServiceInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * ArticleService constructor.
     * @param Article $model
     */
    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    /**
     * @return Builder
     */
    public function getQuery(): Builder
    {
        return $this->model->newQuery();
    }

    /**
     * @param int $id
     * @return Article
     */
    public function get(int $id): ?Article
    {
        return $this->getQuery()->findOrFail($id);
    }

    /**
     * @param array $data
     * @return Paginator
     */
    public function index(array $data): Paginator
    {
        $perPage = Arr::get($data, 'perPage', config('services.articles.paginator.perPage'));
        return $this->getQuery()->paginate($perPage);
    }
}

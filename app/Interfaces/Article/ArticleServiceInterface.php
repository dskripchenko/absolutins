<?php

namespace App\Interfaces\Article;

use App\Models\Article;
use \Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Interface ArticleServiceInterface
 * @package App\Interfaces\Article
 */
interface ArticleServiceInterface
{
    /**
     * @param int $id
     * @return Article
     */
    public function get(int $id): ?Article;

    /**
     * @param array $data
     * @return Collection
     */
    public function index(array $data): Paginator;
}

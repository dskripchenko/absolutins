<?php

namespace App\Http\Resources\Article;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ArticleCollection extends ResourceCollection
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ArticleResource::collection($this->collection),
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \array[][]
     */
    public function with($request)
    {
        return [
            'meta' => [
                'paginator' => [
                    'cntBtns' => config('services.articles.paginator.cntBnts')
                ]
            ]
        ];
    }
}

<?php

namespace App\Http\Resources\Article;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'slug' => $this->slug,
           'title' => $this->title,
           'content' => $this->content,
           'enable' => $this->enable,
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Article\ArticleRequest;
use App\Http\Resources\Article\ArticleCollection;
use App\Http\Resources\Article\ArticleResource;
use App\Interfaces\Article\ArticleServiceInterface;
use Illuminate\Routing\Controller;

/**
 * Class ArticleController
 * @package App\Http\Controllers
 */
class ArticleController extends Controller
{
    /**
     * @var ArticleServiceInterface
     */
    protected $service;

    public function __construct(ArticleServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param ArticleRequest $request
     * @return ArticleCollection
     */
    public function index(ArticleRequest $request): ArticleCollection
    {
        return new ArticleCollection($this->service->index($request->all()));
    }

    /**
     * @param ArticleRequest $request
     * @param int $id
     * @return ArticleResource
     */
    public function get(ArticleRequest $request, int $id): ArticleResource
    {
        return new ArticleResource($this->service->get($id));
    }
}

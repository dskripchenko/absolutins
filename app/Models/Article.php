<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * @property string slug
 * @property string title
 * @property string content
 * @property boolean enable
 *
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 *
 * Class Article
 * @package App\Models
 */
class Article extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'slug', 'title', 'content', 'enable'
    ];

    protected $casts = [
        'enable' => 'boolean'
    ];
}

<?php

use Illuminate\Support\Facades\Route;
use \Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'article', 'as' => 'article.'], function (Router $router) {
    $router->get('/', "ArticleController@index")->name('index');
    $router->get('{id}', "ArticleController@get")->name('get');
});

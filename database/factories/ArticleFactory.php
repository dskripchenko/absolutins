<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'title' => $faker->text(255),
        'content' => $faker->text(1024),
        'enable' => $faker->boolean,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
